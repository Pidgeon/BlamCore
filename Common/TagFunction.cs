using BlamCore.Serialization;

namespace BlamCore.Common
{
    [TagStructure(Size = 0x14)]
    public class TagFunction
    {
        public byte[] Data;
    }
}

