namespace BlamCore.Equipment
{
    public enum GrenadeType : short
    {
        None,
        HumanFragmentation,
        CovenantPlasma,
        BruteClaymore,
        Firebomb
    }
}

