using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Geometry;
using BlamCore.Serialization;
using System;
using System.Collections.Generic;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "render_model", Tag = "mode", Size = 0x1CC, MaxVersion = CacheVersion.Halo3ODST)]
    [TagStructure(Name = "render_model", Tag = "mode", Size = 0x1D0, MinVersion = CacheVersion.HaloOnline106708)]
    public class RenderModel
    {
        public StringId Name;
        public FlagsValue Flags;
        public short Unknown6;
        public int Checksum;
        public List<Region> Regions;
        public int Unknown18;

        public int InstanceStartingMeshIndex;
        public List<InstancePlacement> InstancePlacements;

        public int NodeListChecksum;
        public List<Node> Nodes;

        public List<MarkerGroup> MarkerGroups;
        public List<RenderMaterial> Materials;

        [TagField(Padding = true, Length = 12)]
        public byte[] Unused; // "Errors" block

        public float DontDrawOverCameraCosineAngle;
        public RenderGeometry Geometry = new RenderGeometry();
        public List<TagBlock17> UnknownE8;
        public int UnknownF4;
        public int UnknownF8;
        public int UnknownFC;
        public int Unknown100;
        public int Unknown104;
        public int Unknown108;
        public int Unknown10C;
        public int Unknown110;
        public int Unknown114;
        public int Unknown118;
        public int Unknown11C;
        public int Unknown120;
        public int Unknown124;
        public int Unknown128;
        public int Unknown12C;
        public int Unknown130;
        public int Unknown134;
        public int Unknown138;
        public int Unknown13C;
        public int Unknown140;
        public int Unknown144;
        public int Unknown148;
        public int Unknown14C;
        public int Unknown150;
        public int Unknown154;
        public int Unknown158;
        public int Unknown15C;
        public int Unknown160;
        public int Unknown164;
        public int Unknown168;
        public int Unknown16C;
        public int Unknown170;
        public int Unknown174;
        public int Unknown178;
        public int Unknown17C;
        public int Unknown180;
        public int Unknown184;
        public int Unknown188;
        public int Unknown18C;
        public int Unknown190;
        public int Unknown194;
        public int Unknown198;
        public int Unknown19C;
        public int Unknown1A0;
        public int Unknown1A4;
        public int Unknown1A8;
        public int Unknown1AC;
        public int Unknown1B0;
        public List<TagBlock18> Unknown1B4;
        public List<RuntimeNodeOrientation> RuntimeNodeOrientations;

        [TagField(MinVersion = CacheVersion.HaloOnline106708)]
        public int Unknown1CC;

        [Flags]
        public enum FlagsValue : ushort
        {
            None = 0,
            Bit0 = 1 << 0,
            Bit1 = 1 << 1,
            ForceNodeMaps = 1 << 2,
            Bit3 = 1 << 3,
            Bit4 = 1 << 4,
            Bit5 = 1 << 5,
            Bit6 = 1 << 6,
            Bit7 = 1 << 7,
            Bit8 = 1 << 8,
            Bit9 = 1 << 9,
            Bit10 = 1 << 10,
            Bit11 = 1 << 11,
            Bit12 = 1 << 12,
            Bit13 = 1 << 13,
            Bit14 = 1 << 14,
            Bit15 = 1 << 15
        }

        /// <summary>
        /// A region of a model.
        /// </summary>
        [TagStructure(Size = 0x10)]
        public class Region
        {
            /// <summary>
            /// The name of the region.
            /// </summary>
            public StringId Name;

            /// <summary>
            /// The permutations belonging to the region.
            /// </summary>
            public List<Permutation> Permutations;

            /// <summary>
            /// A permutation of a region, associating a specific mesh with it.
            /// </summary>
            [TagStructure(Size = 0x10, MaxVersion = CacheVersion.Halo3Retail)]
            [TagStructure(Size = 0x18, MinVersion = CacheVersion.Halo3ODST)]
            public class Permutation
            {
                /// <summary>
                /// The name of the permutation as a stringID.
                /// </summary>
                public StringId Name;

                /// <summary>
                /// The index of the first mesh belonging to the permutation.
                /// </summary>
                public short MeshIndex;

                /// <summary>
                /// The number of meshes belonging to the permutation.
                /// </summary>
                public ushort MeshCount;

                public int Unknown8;

                public int UnknownC;

                [TagField(MinVersion = CacheVersion.Halo3ODST)]
                public int Unknown10;

                [TagField(MinVersion = CacheVersion.Halo3ODST)]
                public int Unknown14;
            }
        }

        [Flags]
        public enum InstancePlacementFlags : ushort
        {
            None = 0,
            Bit0 = 1 << 0,
            Bit1 = 1 << 1,
            Bit2 = 1 << 2,
            Bit3 = 1 << 3,
            Bit4 = 1 << 4,
            Bit5 = 1 << 5,
            Bit6 = 1 << 6,
            Bit7 = 1 << 7,
            Bit8 = 1 << 8,
            Bit9 = 1 << 9,
            Bit10 = 1 << 10,
            Bit11 = 1 << 11,
            Bit12 = 1 << 12,
            Bit13 = 1 << 13,
            Bit14 = 1 << 14,
            Bit15 = 1 << 15
        }

        [TagStructure(Size = 0x3C)]
        public class InstancePlacement
        {
            public StringId Name;
            public int NodeIndex;
            public float Scale;
            public RealPoint3d Forward;
            public RealPoint3d Left;
            public RealPoint3d Up;
            public RealPoint3d Position;
        }

        [Flags]
        public enum NodeFlags : ushort
        {
            None = 0,
            ForceDeterministic = 1 << 0,
            ForceRenderThread = 1 << 1
        }

        [TagStructure(Size = 0x60)]
        public class Node
        {
            public StringId Name;
            public short ParentNode;
            public short FirstChildNode;
            public short NextSiblingNode;
            public NodeFlags Flags;
            public RealPoint3d DefaultTranslation;
            public RealQuaternion DefaultRotation;
            public float DefaultScale;
            public RealVector3d InverseForward;
            public RealVector3d InverseLeft;
            public RealVector3d InverseUp;
            public RealPoint3d InversePosition;
            public float DistanceFromParent;
        }

        [TagStructure(Size = 0x10)]
        public class MarkerGroup
        {
            public StringId Name;
            public List<Marker> Markers;

            [TagStructure(Size = 0x24)]
            public class Marker
            {
                public sbyte RegionIndex;
                public sbyte PermutationIndex;
                public sbyte NodeIndex;
                public sbyte Unknown3;
                public RealPoint3d Translation;
                public RealQuaternion Rotation;
                public float Scale;
            }
        }

        [TagStructure(Size = 0x1C)]
        public class TagBlock17
        {
            public float Unknown0;
            public float Unknown4;
            public float Unknown8;
            public float UnknownC;
            public float Unknown10;
            public float Unknown14;
            public float Unknown18;
        }

        [TagStructure(Size = 0x150)]
        public class TagBlock18
        {
            public float Unknown0;
            public float Unknown4;
            public float Unknown8;
            public float UnknownC;
            public float Unknown10;
            public float Unknown14;
            public float Unknown18;
            public float Unknown1C;
            public float Unknown20;
            public float Unknown24;
            public float Unknown28;
            public float Unknown2C;
            public float Unknown30;
            public float Unknown34;
            public float Unknown38;
            public float Unknown3C;
            public float Unknown40;
            public float Unknown44;
            public float Unknown48;
            public float Unknown4C;
            public float Unknown50;
            public float Unknown54;
            public float Unknown58;
            public float Unknown5C;
            public float Unknown60;
            public float Unknown64;
            public float Unknown68;
            public float Unknown6C;
            public float Unknown70;
            public float Unknown74;
            public float Unknown78;
            public float Unknown7C;
            public float Unknown80;
            public float Unknown84;
            public float Unknown88;
            public float Unknown8C;
            public float Unknown90;
            public float Unknown94;
            public float Unknown98;
            public float Unknown9C;
            public float UnknownA0;
            public float UnknownA4;
            public float UnknownA8;
            public float UnknownAC;
            public float UnknownB0;
            public float UnknownB4;
            public float UnknownB8;
            public float UnknownBC;
            public float UnknownC0;
            public float UnknownC4;
            public float UnknownC8;
            public float UnknownCC;
            public float UnknownD0;
            public float UnknownD4;
            public float UnknownD8;
            public float UnknownDC;
            public float UnknownE0;
            public float UnknownE4;
            public float UnknownE8;
            public float UnknownEC;
            public float UnknownF0;
            public float UnknownF4;
            public float UnknownF8;
            public float UnknownFC;
            public float Unknown100;
            public float Unknown104;
            public float Unknown108;
            public float Unknown10C;
            public float Unknown110;
            public float Unknown114;
            public float Unknown118;
            public float Unknown11C;
            public float Unknown120;
            public float Unknown124;
            public float Unknown128;
            public float Unknown12C;
            public float Unknown130;
            public float Unknown134;
            public float Unknown138;
            public float Unknown13C;
            public float Unknown140;
            public float Unknown144;
            public float Unknown148;
            public float Unknown14C;
        }

        [TagStructure(Align = 0x10, Size = 0x20)]
        public class RuntimeNodeOrientation
        {
            public RealQuaternion Rotation;
            public RealPoint3d Translation;
            public float Scale;
        }
    }
}