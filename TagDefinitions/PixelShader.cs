using BlamCore.Common;
using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "pixel_shader", Tag = "pixl", Size = 0x20)]
    public class PixelShader
    {
        public uint Unknown;
        public List<DrawMode> DrawModes;
        public uint Unknown3;
        public List<ShaderBlock> Shaders;

        [TagStructure(Size = 0x2)]
        public class DrawMode
        {
            public byte Index;
            public byte Count;
        }

        [TagStructure(Size = 0x50)]
        public class ShaderBlock
        {
            public byte[] Unknown1;
            public byte[] ByteCode;
            public List<UnknownBlock> Unknown2;
            public uint Unknown3;
            public List<ParameterBlock> Parameters;
            public uint Unknown4;
            public uint Unknown5;
            public uint PixelShader;

            [TagStructure(Size = 0x8)]
            public class UnknownBlock
            {
                public StringId Unknown1;
                public uint Unknown2;
            }

            [TagStructure(Size = 0x8)]
            public class ParameterBlock
            {
                public StringId ParameterName;
                public ushort RegisterIndex;
                public byte RegisterCount;
                public RegisterTypeValue RegisterType;

                public enum RegisterTypeValue : sbyte
                {
                    Boolean,
                    Integer,
                    Float,
                    Sampler,
                    Unknown4,
                    Unknown5,
                    Unknown6,
                    Unknown7
                }
            }
        }
    }
}