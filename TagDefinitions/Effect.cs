using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "effect", Tag = "effe", Size = 0x68, MaxVersion = CacheVersion.Halo3ODST)]
    [TagStructure(Name = "effect", Tag = "effe", Size = 0x70, MinVersion = CacheVersion.HaloOnline106708)]
    public class Effect
    {
        public uint Flags;
        public int Unknown;
        public float Unknown2;
        public uint Unknown3;
        public float Unknown4;
        public sbyte Unknown5;
        public sbyte Unknown6;
        public sbyte Unknown7;
        public sbyte Unknown8;
        public short LoopStartEvent;
        public short Unknown9;
        public uint Unknown10;
        public List<Location> Locations;
        public List<Event> Events;
        public CachedTagInstance LoopingSound;
        public sbyte LocationIndex;
        public sbyte EventIndex;
        public short Unknown11;
        public float AlwaysPlayDistance;
        public float NeverPlayDistance;
        public float Unknown12;
        public float Unknown13;
        public List<UnknownBlock> Unknown14;

        [TagField(Padding = true, Length = 8, MinVersion = CacheVersion.HaloOnline106708)]
        public byte[] Unused;

        [TagStructure(Size = 0xC)]
        public class Location
        {
            public StringId MarkerName;
            public int Unknown;
            public sbyte Unknown2;
            public sbyte Unknown3;
            public sbyte Unknown4;
            public sbyte Unknown5;
        }

        [TagStructure(Size = 0x44)]
        public class Event
        {
            public StringId Name;
            public int Unknown;
            public sbyte Unknown2;
            public sbyte Unknown3;
            public sbyte Unknown4;
            public sbyte Unknown5;
            public float SkipFraction;
            public float DelayBoundsMin;
            public float DelayBoundsMax;
            public float DurationBoundsMin;
            public float DurationBoundsMax;
            public List<Part> Parts;
            public List<Acceleration> Accelerations;
            public List<ParticleSystem> ParticleSystems;

            [TagStructure(Size = 0x60)]
            public class Part
            {
                public CreateInEnvironmentValue CreateInEnvironment;
                public CreateInDispositionValue CreateInDisposition;
                public short LocationIndex;
                public short Unknown;
                public short Unknown2;
                public sbyte Unknown3;
                public CameraModeValue CameraMode;
                public int AnticipatedTagClass;
                public CachedTagInstance SpawnedTag;
                public float VelocityBoundsMin;
                public float VelocityBoundsMax;
                public uint Unknown4;
                public uint Unknown5;
                public Angle VelocityConeAngle;
                public Angle AngularVelocityBoundsMin;
                public Angle AngularVelocityBoundsMax;
                public float RadiusModifierBoundsMin;
                public float RadiusModifierBoundsMax;
                public float OriginOffsetX;
                public float OriginOffsetY;
                public float OriginOffsetZ;
                public Angle OriginRotationI;
                public Angle OriginRotationJ;
                public uint AScalesValues;
                public uint BScalesValues;

                public enum CreateInEnvironmentValue : short
                {
                    AnyEnvironment,
                    AirOnly,
                    WaterOnly,
                    SpaceOnly,
                }

                public enum CreateInDispositionValue : short
                {
                    EitherMode,
                    ViolentModeOnly,
                    NonviolentModeOnly,
                }

                public enum CameraModeValue : sbyte
                {
                    IndependentOfCameraMode,
                    FirstPersonOnly,
                    ThirdPersonOnly,
                    BothFirstAndThird,
                }
            }

            [TagStructure(Size = 0x14)]
            public class Acceleration
            {
                public CreateInEnvironmentValue CreateInEnvironment;
                public CreateInDispositionValue CreateInDisposition;
                public short LocationIndex;
                public short Unknown;
                public float Acceleration2;
                public float InnerConeAngle;
                public float OuterConeAngle;

                public enum CreateInEnvironmentValue : short
                {
                    AnyEnvironment,
                    AirOnly,
                    WaterOnly,
                    SpaceOnly,
                }

                public enum CreateInDispositionValue : short
                {
                    EitherMode,
                    ViolentModeOnly,
                    NonviolentModeOnly,
                }
            }

            [TagStructure(Size = 0x5C)]
            public class ParticleSystem
            {
                public sbyte Unknown;
                public sbyte Unknown2;
                public sbyte Unknown3;
                public sbyte Unknown4;
                public CachedTagInstance Particle;
                public uint LocationIndex;
                public CoordinateSystemValue CoordinateSystem;
                public EnvironmentValue Environment;
                public DispositionValue Disposition;
                public CameraModeValue CameraMode;
                public short SortBias;
                public ushort Flags;
                public float Unknown6;
                public float Unknown7;
                public float Unknown8;
                public float Unknown9;
                public uint Unknown10;
                public float Unknown11;
                public float AmountSize;
                public float Unknown12;
                public float LodInDistance;
                public float LodFeatherInDelta;
                public List<Emitter> Emitters;
                public float Unknown13;

                public enum CoordinateSystemValue : short
                {
                    World,
                    Local,
                    Parent,
                }

                public enum EnvironmentValue : short
                {
                    AnyEnvironment,
                    AirOnly,
                    WaterOnly,
                    SpaceOnly,
                }

                public enum DispositionValue : short
                {
                    EitherMode,
                    ViolentModeOnly,
                    NonviolentModeOnly,
                }

                public enum CameraModeValue : short
                {
                    IndependentOfCameraMode,
                    FirstPersonOnly,
                    ThirdPersonOnly,
                    BothFirstAndThird,
                }

                [TagStructure(Size = 0x2F0, MaxVersion = CacheVersion.Halo3Retail)]
                [TagStructure(Size = 0x300, MinVersion = CacheVersion.Halo3ODST)]
                public class Emitter
                {
                    public StringId Name;
                    public ushort Unknown;
                    public short Unknown2;

                    [TagField(MinVersion = CacheVersion.Halo3ODST)]
                    public CachedTagInstance CustomEmitterPoints;

                    public uint Unknown3;
                    public uint Unknown4;
                    public uint Unknown5;
                    public uint Unknown6;
                    public TagMapping Function1;
                    public uint Unknown10;
                    public uint Unknown11;
                    public uint Unknown12;
                    public uint Unknown13;
                    public uint Unknown14;
                    public uint Unknown15;
                    public TagMapping Function2;
                    public uint Unknown19;
                    public uint Unknown20;
                    public uint Unknown21;
                    public uint Unknown22;
                    public uint Unknown23;
                    public uint Unknown24;
                    public TagMapping Function3;
                    public TagMapping Function4;
                    public TagMapping Function5;
                    public TagMapping Function6;
                    public TagMapping Function7;
                    public TagMapping Function8;
                    public TagMapping Function9;

                    public CachedTagInstance ParticlePhysics;

                    public uint Unknown46;

                    public List<UnknownBlock> Unknown47;

                    public TagMapping Function10;
                    public uint Unknown51;
                    public uint Unknown52;
                    public uint Unknown53;
                    public uint Unknown54;
                    public uint Unknown55;
                    public uint Unknown56;
                    public TagMapping Function11;
                    public TagMapping Function12;
                    public TagMapping Function13;
                    public TagMapping Function14;
                    public TagMapping Function15;
                    public TagMapping Function16;
                    public TagMapping Function17;
                    public TagMapping Function18;
                    public int Unknown77;
                    public int Unknown78;
                    public int Unknown79;
                    public List<UnknownBlock2> Unknown80;
                    public List<CompiledFunction> CompiledFunctions;
                    public List<CompiledColorValue> CompiledColorValues;

                    [TagStructure(Size = 0x18)]
                    public class UnknownBlock
                    {
                        public short Unknown1;
                        public short Unknown2;
                        public List<UnknownBlock2> Unknown3;
                        public int Unknown4;
                        public uint Unknown5;

                        [TagStructure(Size = 0x24)]
                        public class UnknownBlock2
                        {
                            public int Unknown;
                            public TagMapping Function;
                        }
                    }

                    [TagStructure(Size = 0x10)]
                    public class UnknownBlock2
                    {
                        public uint Unknown;
                        public uint Unknown2;
                        public uint Unknown3;
                        public uint Unknown4;
                    }

                    [TagStructure(Size = 0x40)]
                    public class CompiledFunction
                    {
                        public uint Unknown;
                        public uint Unknown2;
                        public uint Unknown3;
                        public uint Unknown4;
                        public uint Unknown5;
                        public uint Unknown6;
                        public uint Unknown7;
                        public uint Unknown8;
                        public uint Unknown9;
                        public uint Unknown10;
                        public uint Unknown11;
                        public uint Unknown12;
                        public uint Unknown13;
                        public uint Unknown14;
                        public uint Unknown15;
                        public uint Unknown16;
                    }

                    [TagStructure(Size = 0x10)]
                    public class CompiledColorValue
                    {
                        public RealRgbColor Color;
                        public float Magnitude;
                    }
                }
            }
        }

        [TagStructure(Size = 0xC)]
        public class UnknownBlock
        {
            public uint Unknown;
            public uint Unknown2;
            public uint Unknown3;
        }
    }
}

