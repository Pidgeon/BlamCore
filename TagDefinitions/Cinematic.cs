using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "cinematic", Tag = "cine", Size = 0xB0 , MaxVersion = CacheVersion.Halo3Retail)]
    [TagStructure(Name = "cinematic", Tag = "cine", Size = 0xB4, MinVersion = CacheVersion.Halo3ODST)]
    public class Cinematic
    {
        public uint Unknown1;

        [TagField(MinVersion = CacheVersion.Halo3ODST)]
        public uint Unknown2;

        public List<uint> SceneIndices;
        public CachedTagInstance ImportScenario;
        public int Unknown3;
        public StringId ScenarioName; 
        public short Unknown4;
        public short Unknown5;
        public int Unknown6;
        public int Unknown7;
        public int Unknown8;
        public int Unknown9;
        public int Unknown10;
        public int Unknown11;
        public int Unknown12;
        public int Unknown13;
        public int Unknown14;
        public int Unknown15;
        public int Unknown16;

        public CachedTagInstance Unknown17;

        //Scripts are in ASCIIZ format, they will probably need conversion to work in HO

        public byte[] ImportScript1;

        public List<CachedTagInstance> CinematicScenes;

        public byte[] ImportScript2;
        public byte[] ImportScript3;
    }
}

