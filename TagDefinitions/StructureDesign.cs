using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Geometry;
using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "structure_design", Tag = "sddt", Size = 0x40, MaxVersion = CacheVersion.Halo3Retail)]
    [TagStructure(Name = "structure_design", Tag = "sddt", Size = 0x44, MinVersion = CacheVersion.Halo3ODST)]
    public class StructureDesign
    {
        public int Unknown;

        public List<CollisionMoppCode> DesignMoppCodes;
        public List<DesignShapes2Block> DesignShapes2;
        public List<WaterMoppCode> WaterMoppCodes;
        public List<WaterName> WaterNames;
        public List<UnderwaterDefinition> UnderwaterDefinitions;

        [TagField(MinVersion = CacheVersion.Halo3ODST)]
        public uint Unknown2;

        [TagStructure(Size = 0x14)]
        public class DesignShapes2Block
        {
            public StringId Name;
            public short Unknown;
            public short Unknown2;
            public List<Unknown2Block> Unknown2_2;

            [TagStructure(Size = 0x44)]
            public class Unknown2Block
            {
                public uint Unknown;
                public uint Unknown2;
                public uint Unknown3;
                public uint Unknown4;
                public uint Unknown5;
                public uint Unknown6;
                public uint Unknown7;
                public uint Unknown8;
                public uint Unknown9;
                public uint Unknown10;
                public uint Unknown11;
                public uint Unknown12;
                public uint Unknown13;
                public uint Unknown14;
                public uint Unknown15;
                public uint Unknown16;
                public uint Unknown17;
            }
        }

        [TagStructure(Size = 0x4)]
        public class WaterName
        {
            public StringId Name;
        }

        [TagStructure(Size = 0x2C)]
        public class UnderwaterDefinition
        {
            public short WaterNameIndex;
            public short Unknown;
            public float FlowForceX;
            public float FlowForceY;
            public float FlowForceZ;
            public float FlowForceZ2;
            public List<UnknownBlock> Unknown2;
            public List<Triangle> Triangles;

            [TagStructure(Size = 0x10)]
            public class UnknownBlock
            {
                public uint Unknown;
                public uint Unknown2;
                public uint Unknown3;
                public uint Unknown4;
            }

            [TagStructure(Size = 0x24)]
            public class Triangle
            {
                public float _1X;
                public float _1Y;
                public float _1Z;
                public float _2X;
                public float _2Y;
                public float _2Z;
                public float _3X;
                public float _3Y;
                public float _3Z;
            }
        }
    }
}