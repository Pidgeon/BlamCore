using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;
using BlamCore.Shaders;
using System.Collections.Generic;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "vertex_shader", Tag = "vtsh", Size = 0x20, MinVersion = CacheVersion.Halo3Retail)]
	public class VertexShader
	{
		public uint Unknown;
		public List<UnknownBlock> Unknown2;
		public uint Unknown3;
		public List<ShaderBlock> Shaders;

		[TagStructure(Size = 0xC)]
		public class UnknownBlock
		{
			public List<DrawMode> DrawModes;

			[TagStructure(Size = 0x2)]
			public class DrawMode
			{
				public short Unknown;
			}
		}

		[TagStructure(Size = 0x50)]
		public class ShaderBlock
		{
			public byte[] Unknown;
			public byte[] ByteCode;
			public uint Unknown3;
			public uint Unknown4;
			public uint Unknown5;
			public uint Unknown6;
			public List<ParameterBlock> Parameters;
			public uint Unknown8;
			public uint Unknown9;
			public uint ShaderReference;

			[TagStructure(Size = 0x8)]
			public class ParameterBlock
			{
				public StringId ParameterName;
				public short RegisterIndex;
				public byte RegisterCount;
				public RType RegisterType;

				public enum RType : byte
				{
					Boolean = 0,
					Integer = 1,
					Float = 2,
					Sampler = 3
				};
			}
		}
	}
}