using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;
using System;
using System.Collections.Generic;

namespace BlamCore.Geometry
{
    [TagStructure(Size = 0x44, MaxVersion = CacheVersion.Halo2Vista)]
    [TagStructure(Size = 0x5C, MinVersion = CacheVersion.Halo3Retail)]
    public class CollisionGeometry
    {
        public List<Bsp3dNode> Bsp3dNodes;
        public List<Plane> Planes;
        public List<Leaf> Leaves;
        public List<Bsp2dReference> Bsp2dReferences;
        public List<Bsp2dNode> Bsp2dNodes;
        public List<Surface> Surfaces;
        public List<Edge> Edges;
        public List<Vertex> Vertices;

        [TagField(MaxVersion = CacheVersion.Halo2Vista)]
        public float Unknown;

        [TagStructure(Size = 0x8, Align = 0x8)]
        public class Bsp3dNode
        {
            //
            // TODO: Fix this nasty mess. In the tag definition, it's an int64.
            //

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public short Plane_H2;
            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte FrontChildLower_H2;
            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte FrontChildMid_H2;
            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte FrontChildUpper_H2;
            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte BackChildLower_H2;
            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte BackChildMid_H2;
            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte BackChildUpper_H2;

            [TagField(MaxVersion = CacheVersion.Halo3ODST)]
            public byte BackChildUpper_H3;
            [TagField(MaxVersion = CacheVersion.Halo3ODST)]
            public byte BackChildMid_H3;
            [TagField(MaxVersion = CacheVersion.Halo3ODST)]
            public byte BackChildLower_H3;
            [TagField(MaxVersion = CacheVersion.Halo3ODST)]
            public byte FrontChildUpper_H3;
            [TagField(MaxVersion = CacheVersion.Halo3ODST)]
            public byte FrontChildMid_H3;
            [TagField(MaxVersion = CacheVersion.Halo3ODST)]
            public byte FrontChildLower_H3;
            [TagField(MaxVersion = CacheVersion.Halo3ODST)]
            public short Plane_H3;

            [TagField(MinVersion = CacheVersion.HaloOnline106708)]
            public short Plane;
            [TagField(MinVersion = CacheVersion.HaloOnline106708)]
            public byte FrontChildLower;
            [TagField(MinVersion = CacheVersion.HaloOnline106708)]
            public byte FrontChildMid;
            [TagField(MinVersion = CacheVersion.HaloOnline106708)]
            public byte FrontChildUpper;
            [TagField(MinVersion = CacheVersion.HaloOnline106708)]
            public byte BackChildLower;
            [TagField(MinVersion = CacheVersion.HaloOnline106708)]
            public byte BackChildMid;
            [TagField(MinVersion = CacheVersion.HaloOnline106708)]
            public byte BackChildUpper;
        }

        [TagStructure(Size = 0x10, Align = 0x10)]
        public class Plane
        {
            public RealPlane3d Value;
        }

        [Flags]
        public enum LeafFlags : byte
        {
            None = 0,
            ContainsDoubleSidedSurfaces = 1 << 0
        }

        [TagStructure(Size = 0x4, MaxVersion = CacheVersion.Halo2Vista)]
        [TagStructure(Size = 0x8, MinVersion = CacheVersion.Halo3Retail)]
        public class Leaf
        {
            public LeafFlags Flags;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public LeafFlags Flags2;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte Bsp2dReferenceCount_H2;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public short Bsp2dReferenceCount;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public short FirstBsp2dReference_H2;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public int FirstBsp2dReference;
        }

        [TagStructure(Size = 0x4)]
        public class Bsp2dReference
        {
            public short PlaneIndex;
            public short Bsp2dNodeIndex;
        }

        [TagStructure(Size = 0x10, Align = 0x10)]
        public class Bsp2dNode
        {
            public RealPlane2d Plane;
            public short LeftChild;
            public short RightChild;
        }

        [Flags]
        public enum SurfaceFlags : byte
        {
            None = 0,
            TwoSided = 1 << 0,
            Invisible = 1 << 1,
            Climbable = 1 << 2,
            Invalid = 1 << 3,
            Conveyor = 1 << 4,
            Slip = 1 << 5,
            PlaneNegated = 1 << 6
        }

        [TagStructure(Size = 0x8, MaxVersion = CacheVersion.Halo2Vista)]
        [TagStructure(Size = 0xC, MinVersion = CacheVersion.Halo3Retail)]
        public class Surface
        {
            public short Plane;

            public short FirstEdge;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public SurfaceFlags Flags_H2;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public short MaterialIndex;
            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public short Unknown;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public byte BreakableSurfaceIndex_H2;
            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public short BreakableSurfaceIndex;

            [TagField(MaxVersion = CacheVersion.Halo2Vista)]
            public short MaterialIndex_H2;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public SurfaceFlags Flags;

            [TagField(MinVersion = CacheVersion.Halo3Retail)]
            public byte BestPlaneCalculationVertex;
        }

        [TagStructure(Size = 0xC)]
        public class Edge
        {
            public short StartVertex;
            public short EndVertex;
            public short ForwardEdge;
            public short ReverseEdge;
            public short LeftSurface;
            public short RightSurface;
        }

        [TagStructure(Size = 0x10, Align = 0x10)]
        public class Vertex
        {
            public RealPoint3d Point;
            public short FirstEdge;
            public short Sink;
        }
    }
}

