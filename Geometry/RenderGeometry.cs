using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;
using System;
using System.Collections.Generic;

namespace BlamCore.Geometry
{
    [TagStructure(Name = "render_geometry", Size = 0x84)]
    public class RenderGeometry
    {
        /// <summary>
        /// The runtime flags of the render geometry.
        /// </summary>
        public RenderGeometryRuntimeFlags RuntimeFlags;

        /// <summary>
        /// The meshes of the render geometry.
        /// </summary>
        public List<Mesh> Meshes;

        /// <summary>
        /// The compression information of the render geometry.
        /// </summary>
        public List<GeometryCompressionInfo> Compression;

        /// <summary>
        /// The bounding spheres of the render geometry.
        /// </summary>
        public List<BoundingSphere> BoundingSpheres;

        public List<UnknownBlock> Unknown2;

        public uint Unknown3;
        public uint Unknown4;
        public uint Unknown5;

        public List<UnknownSection> UnknownSections;

        /// <summary>
        /// The per-mesh node mappings of the render geometry.
        /// </summary>
        public List<PerMeshNodeMap> PerMeshNodeMaps;

        /// <summary>
        /// The per-mesh subpart visibility of the render geometry.
        /// </summary>
        public List<PerMeshSubpartVisibilityBlock> PerMeshSubpartVisibility;

        public uint Unknown7;
        public uint Unknown8;
        public uint Unknown9;

        /// <summary>
        /// The per-mesh level-of-detail data of the render geometry.
        /// </summary>
        public List<PerMeshLodDatum> PerMeshLodData;
        
        /// <summary>
        /// The resource containing the raw geometry data.
        /// </summary>
        [TagField(Pointer = true, MinVersion = CacheVersion.HaloOnline106708)]
        public PageableResource Resource;

        /// <summary>
        /// The index of the resource entry in the cache_file_resource_gestalt tag.
        /// </summary>
        [TagField(MaxVersion = CacheVersion.Halo3ODST)]
        public int ZoneAssetHandle;
        
        [TagField(Padding = true, Length = 4)]
        public byte[] Unused;

        [TagStructure(Size = 0x30)]
        public class BoundingSphere
        {
            public RealPlane3d Plane;
            public RealPoint3d Position;
            public float Radius;

            [TagField(Length = 4)]
            public sbyte[] NodeIndices;

            [TagField(Length = 3)]
            public float[] NodeWeights;
        }

        [TagStructure(Size = 0x18)]
        public class UnknownBlock
        {
            public byte UnknownByte1;
            public byte UnknownByte2;
            public short Unknown2;
            public byte[] Unknown3;
        }

        [TagStructure(Size = 0x20)]
        public class UnknownSection
        {
            [TagField(Align = 0x10)]
            public byte[] Unknown;

            public List<short> Unknown2;
        }

        [TagStructure(Size = 0xC)]
        public class PerMeshNodeMap
        {
            public List<byte> NodeIndices;
        }

        [TagStructure(Size = 0xC)]
        public class PerMeshSubpartVisibilityBlock
        {
            public List<BoundingSphere> BoundingSpheres;
        }

        [TagStructure(Size = 0x10)]
        public class PerMeshLodDatum
        {
            public List<int> Indices;

            public short VertexBufferIndex;

            [TagField(Padding = true, Length = 2)]
            public byte[] Unused;
        }
    }

    [Flags]
    public enum RenderGeometryRuntimeFlags : int
    {
        None = 0,
        Processed = 1 << 0,
        Available = 1 << 1,
        HasValidBudgets = 1 << 2,
        ManualResourceCalibration = 1 << 3,
        KeepRawGeometry = 1 << 4,
        DoNotUseCompressedVertexPositions = 1 << 5,
        PcaAnimationTableSorted = 1 << 6,
        HasDualQuat = 1 << 7
    }

    [Flags]
    public enum GeometryCompressionFlags : ushort
    {
        None = 0,
        CompressedPosition = 1 << 0,
        CompressedTexcoord = 1 << 1,
        CompressionOptimized = 1 << 2
    }

    /// <summary>
    /// Contains information about how geometry is compressed.
    /// </summary>
    [TagStructure(Size = 0x2C)]
    public class GeometryCompressionInfo
    {
        /// <summary>
        /// The flags of the geometry compression.
        /// </summary>
        public GeometryCompressionFlags Flags;

        [TagField(Padding = true, Length = 0x2)]
        public byte[] Padding;

        /// <summary>
        /// The minimum X value in the uncompressed geometry.
        /// </summary>
        public Bounds<float> X;

        /// <summary>
        /// The minimum Y value in the uncompressed geometry.
        /// </summary>
        public Bounds<float> Y;

        /// <summary>
        /// The minimum Z value in the uncompressed geometry.
        /// </summary>
        public Bounds<float> Z;

        /// <summary>
        /// The minimum U value in the uncompressed geometry.
        /// </summary>
        public Bounds<float> U;

        /// <summary>
        /// The minimum V value in the uncompressed geometry.
        /// </summary>
        public Bounds<float> V;
    }
}
