using BlamCore.Common;
using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.Geometry
{
    [TagStructure(Size = 0x40)]
    public class CollisionMoppCode
    {
        public int Unused1;
        public short Size;
        public short Count;
        public int Address;
        public int Unused2;
        public RealQuaternion Offset;
        public int Unused3;
        public int DataSize;
        public int DataCapacityAndFlags;
        public sbyte DataBuildType;
        public sbyte Unused4;
        public short Unused5;
        public List<byte> Data;
        public sbyte MoppBuildType;
        public byte Unused6;
        public short Unused7;
    }

    [TagStructure(Size = 0x40)]
    public class WaterMoppCode
    {
        public int Unused1;
        public short Size;
        public short Count;
        public int Address;
        public int Unused2;
        public RealQuaternion Offset;
        public int Unused3;
        public int DataSize;
        public int DataCapacityAndFlags;
        public sbyte DataBuildType;
        public sbyte Unused4;
        public short Unused5;

        [TagField(MaxVersion = Cache.CacheVersion.Halo3ODST)]
        public List<byte> Data_H3;
        [TagField(MinVersion = Cache.CacheVersion.HaloOnline106708)]
        public List<ushort> Data_HO;

        public sbyte MoppBuildType;
        public byte Unused6;
        public short Unused7;
    }
}
