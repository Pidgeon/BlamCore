using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x18)]
    public class AiMissionDialogueLineVariant
    {
        public StringId Designation;
        public CachedTagInstance Sound;
        public StringId SoundEffect;
    }
}

