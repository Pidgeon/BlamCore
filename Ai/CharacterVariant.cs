using BlamCore.Common;
using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x14)]
    public class CharacterVariant
    {
        public StringId VariantName;
        public short VariantIndex;

        [TagField(Padding = true, Length = 2)]
        public byte[] Unused;

        public List<CharacterDialogueVariation> DialogueVariations;
    }
}

