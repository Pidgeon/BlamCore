using System;

namespace BlamCore.Ai
{
    [Flags]
    public enum CharacterRetreatFlags : int
    {
        None = 0,
        ZigZagWhenFleeing = 1 << 0
    }
}

