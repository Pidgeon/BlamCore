namespace BlamCore.Ai
{
    public enum AiFollowerPositioning : short
    {
        InFrontOfMe,
        BehindMe,
        Tight
    }
}

