using BlamCore.Common;
using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x14)]
    public class AiMissionDialogueLine
    {
        public StringId Name;
        public List<AiMissionDialogueLineVariant> Variants;
        public StringId DefaultSoundEffect;
    }
}

