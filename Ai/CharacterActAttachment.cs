using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x1C)]
    public class CharacterActAttachment
    {
        public StringId Name;
        public CachedTagInstance ChildObject;
        public StringId ChildMarker;
        public StringId ParentMarker;
    }
}

