using BlamCore.Serialization;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0xC)]
    public class CharacterVocalizationProperties
    {
        public float CharacterSkipFraction;
        public float LookCommentTime;
        public float LookLongCommentTime;
    }
}

