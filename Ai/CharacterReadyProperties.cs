using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x8)]
    public class CharacterReadyProperties
    {
        public Bounds<float> ReadTimeBounds;
    }
}

