using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x28, MaxVersion = CacheVersion.Halo3Retail)]
    [TagStructure(Size = 0x38, MinVersion = CacheVersion.Halo3ODST)]
    public class CharacterEngageProperties
    {
        public CharacterEngageFlags Flags;
        public uint Unknown;
        public float CrouchDangerThreshold;
        public float StandDangerThreshold;
        public float FightDangerMoveThreshold;
        public Bounds<short> FightDangerMoveThresholdCooldown;
        public CachedTagInstance OverrideGrenadeProjectile;

        [TagField(MinVersion = CacheVersion.Halo3ODST)]
        public uint Unknown4;
        [TagField(MinVersion = CacheVersion.Halo3ODST)]
        public uint Unknown5;
        [TagField(MinVersion = CacheVersion.Halo3ODST)]
        public uint Unknown6;
        [TagField(MinVersion = CacheVersion.Halo3ODST)]
        public uint Unknown7;
    }
}

