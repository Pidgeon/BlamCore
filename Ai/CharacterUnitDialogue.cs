using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0xC)]
    public class CharacterUnitDialogue
    {
        public List<CharacterDialogueVariation> DialogueVariations;
    }
}

