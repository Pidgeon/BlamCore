using BlamCore.Cache;
using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x1C)]
    public class CharacterFiringPatternProperties
    {
        public CachedTagInstance Weapon;
        public List<CharacterFiringPattern> FiringPatterns;
    }
}

