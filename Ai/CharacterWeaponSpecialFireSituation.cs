namespace BlamCore.Ai
{
    public enum CharacterWeaponSpecialFireSituation : short
    {
        Never,
        EnemyVisible,
        EnemyOutOfSight,
        Strafing
    }
}

